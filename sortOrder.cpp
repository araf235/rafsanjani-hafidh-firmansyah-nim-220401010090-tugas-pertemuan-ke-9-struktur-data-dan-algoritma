#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

bool urutanKota(const string& namaAwal, const string& namaAkhir) {
    return namaAwal[0] < namaAkhir[0];
}

int main() {
    const int hurufAwal = 7;
    string namaKota[hurufAwal] = {"Jakarta", "Malang", "Surabaya", "Bandung", "Garut", "Depok", "Tangerang"};

    // Mengurutkan array menggunakan fungsi sort
    sort(namaKota, namaKota + hurufAwal, urutanKota);

    // Menampilkan array yang telah diurutkan
    for (int i = 0; i < hurufAwal; i++) {
        cout << namaKota[i] << endl;
    }

    return 0;
}
